var path = require('path');

module.exports = {
    entry: './client.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'client.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        }]
    },
    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    }    
}