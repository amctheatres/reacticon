import React from 'react';
import ReactDOM from 'react-dom';
import Icon, { initializeIcons } from '../lib/Reacticon';
import 'babel-polyfill';

initializeIcons(
    'icon/_generated/icons.data.svg.css',
    'icon/_generated/icons.data.png.css',
    'icon/_generated/icons.fallback.css'
);

ReactDOM.render(
    <div>
        <div>
            <p>Look, it&apos;s a reacticon!</p>
            <Icon className='icon_cocktail' />
        </div>
        <div>
            <p>IE Tab Test</p>
            <input type="text" />
            <Icon className='icon_cocktail' />
            <input type="text" />
        </div>
    </div>,
    document.getElementById('reacticon')
);
