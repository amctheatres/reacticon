import express from 'express';
import path from 'path';

const port = 3003;

const app = express();
app.use(express.static(path.join(__dirname, 'public')));

app.listen(port, () => console.log(`listening on port ${port}`));