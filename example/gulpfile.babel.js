import gulp from 'gulp';
import gulpicon from 'gulpicon/tasks/gulpicon';

let config = {
    dest: './public/icon/_generated',
    enhanceSVG: true,
    width: '50px',
    height: '50px',
    cssprefix: '.'
};

gulp.task('icon', gulpicon(['./public/icon/icon_cocktail.svg'], config));