module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "arrow-body-style": [1, "as-needed"],
        "arrow-parens": [1, "as-needed"],
        "class-methods-use-this": 0,
        "comma-dangle": 0,
        "dot-notation": 0,
        "eol-last": 1,
        "generator-star-spacing": 0,
        "global-require": 0,
        "import/no-named-as-default": 0,
        "indent": [2, 4, { "SwitchCase": 1 }],
        "linebreak-style": 0,
        "max-len": [2, 150, 4, {"ignoreUrls": true, "ignoreTrailingComments": true, "ignorePattern": "(Error|logger)"}],
        "no-console": 1,
        "no-continue": 1,
        "no-debugger": 1,
        "no-global-assign": 0,
        "no-mixed-requires": [0],
        "no-multiple-empty-lines": [1, { "max": 1, "maxEOF": 0, "maxBOF": 0 }],
        "no-param-reassign": 0,
        "no-plusplus": 0,
        "no-restricted-syntax": 0,
        "no-tabs": 1,
        "no-trailing-spaces": 1,
        "no-underscore-dangle": [0],
        "no-unsafe-negation": 0,
        "no-unused-vars": 1,
        "no-use-before-define": 0,
        "operator-linebreak": [1, "before"],
        "quotes": [2, "single"],
        "react/jsx-closing-bracket-location": 1,
        "react/jsx-curly-spacing": 1,
        "react/jsx-indent": 1,
        "react/jsx-indent-props": 1,
        "react/jsx-key": 2,
        "react/jsx-no-target-blank": 2,
        "react/jsx-tag-spacing": [1, { "afterOpening": "never", "beforeSelfClosing": "always" }],
        "react/jsx-wrap-multilines": 0, // TODO: change to warn
        "react/no-deprecated": 2,
        "react/no-direct-mutation-state": 2,
        "react/no-find-dom-node": 2,
        "react/no-string-refs": 0, // TODO: change to error
        "react/no-unescaped-entities": 2,
        "react/prop-types": 2        
    },
    "settings": {
        "react": {
            "version": "detect"
        }
    }
};