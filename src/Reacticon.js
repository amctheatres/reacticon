import React from 'react';
import PropTypes from 'prop-types';
import {
    grunticon,
    addWork,
    isPopulated,
    lookupIcon
} from './grunticon';

let tagName = 'i';

const initializeIcons = (datasvg, datapng, urlpng, defaultTagName = 'i') => {
    tagName = defaultTagName;

    grunticon({
        datasvg,
        datapng,
        urlpng
    });
}

export default class Reacticon extends React.Component {
    // this is kept for backward compatibility ... initializeIcons() should be called directly
    static initialize(datasvg, datapng, urlpng, defaultTagName = 'i') {
        initializeIcons(datasvg, datapng, urlpng, defaultTagName);
    }

    constructor(props) {
        super(props);

        this.state = { svg: null };
    }

    componentDidMount() {
        this.setSvg();
    }

    componentDidUpdate(prevProps) {
        const { className = '' } = prevProps;

        if (className === this.props.className) return;

        this.setSvg();
    }

    setSvg() {
        const { className = '' } = this.props;

        if (isPopulated()) {
            this.getIcon(className);
        } else {
            addWork(() => this.getIcon(className))
        }
    }

    getIcon(className) {
        this.setState({ svg: lookupIcon(className) });
    }

    render() {
        const { svg } = this.state;
        const newProps = svg
            ? Object.assign({}, this.props, {
                dangerouslySetInnerHTML: { __html: svg },
                style: { backgroundImage: 'none' }
            })
            : { ...this.props };

        return React.createElement(this.props.tagName || tagName, newProps);
    }
}

Reacticon.propTypes = {
    tagName: PropTypes.string,
    className: PropTypes.string,
};

export { initializeIcons };
