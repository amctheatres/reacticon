const getInsertTarget = doc => {
    const nodes = (doc.body || doc.getElementsByTagName('head')[0]).childNodes;

    return nodes[nodes.length - 1];
};

const getStyleSheetElement = (doc, attrs = {}) => {
    const element = doc.createElement('link');

    Object.entries(attrs).forEach(([key, value]) => element.setAttribute(key, value));

    return element;
};

const addLinkToDocument = (insertTarget, stylesheet) => {
    if (insertTarget && insertTarget.parentNode) {
        insertTarget.parentNode.insertBefore(stylesheet, insertTarget.nextSibling);
    }
};

const onCssLoaded = (doc, href) => callback => {
    const { styleSheets } = doc;
    let { length: i } = styleSheets;

    while (i--) {
        if (styleSheets[i].href === href) {
            return callback();
        }
    }

    setTimeout(() => onCssLoaded(doc, href)(callback));
};

const onBodyLoaded = (doc, callback) => {
    if (doc.body) {
        return callback();
    }

    setTimeout(() => onBodyLoaded(doc, callback));
};

const getCSS = href => window && window.document ? window.document.querySelector('link[href$="'+ href +'"]') : null;

const cssLoader = (href, target, media, attributes) => {
    if (typeof window === 'undefined') return {};

    const { document: doc } = window;
    const styleSheetElement = getStyleSheetElement(doc, Object.assign({}, { href, rel: 'stylesheet', media: 'no media' }, attributes));
    const onCssLoad = onCssLoaded(doc, href);

    // register our callback to be used when the body is loaded : add link element to the body
    onBodyLoaded(doc, addLinkToDocument.bind(null, target || getInsertTarget(doc), styleSheetElement));

    // ====================================
    // style sheet loading events
    const cssLoadedCallback = () => {
        if (styleSheetElement.addEventListener) {
            styleSheetElement.removeEventListener('load', cssLoadedCallback);
        }

        // once loaded, set link's media back to `all` so that the stylesheet applies once it loads
        styleSheetElement.media = media || 'all';
    };

    if (styleSheetElement.addEventListener) {
        styleSheetElement.addEventListener('load', cssLoadedCallback);
    }

    // A method (exposed on return object for external use) that mimics onload by polling document.styleSheets until it includes the new sheet.
    styleSheetElement.onloadcssdefined = onCssLoad;

    // register our callback to be used when the css is loaded :  set the media to specified value or all
    onCssLoad(cssLoadedCallback);

    return styleSheetElement;
}

export { cssLoader, getCSS };
