import { cssLoader, getCSS } from './cssLoader';

// Largely borrowed then modified from Grunticon:
// https://github.com/filamentgroup/grunticon-lib/blob/master/static/grunticon.loader.js
// https://github.com/filamentgroup/grunticon-lib/blob/master/static/grunticon.embed.js

let iconStore = null;
const selectorPlaceholder = 'grunticon:';
const deferredWork = [];

const addWork = work => deferredWork.push(work);

const isPopulated = () => !!(iconStore);

const lookupIcon = className => {
    const classes = (className || '').split(' ');

    if (iconStore) {
        for (let classy of classes) {
            const searchKey = `${selectorPlaceholder}.${classy.trim()}`;

            if (iconStore[searchKey]) {
                return iconStore[searchKey];
            }
        }
    }

    return null;
};

const getIcons = stylesheet => {
    let icons = {},
        svgss,
        rules, cssText,
        iconClass,
        iconSVGEncoded,
        iconSVGRaw;

    svgss = stylesheet.sheet;

    if (!svgss) return icons;

    rules = svgss.cssRules ? svgss.cssRules : svgss.rules;

    if (!rules || !rules.length) return icons;

    for (let i = 0; i < rules.length; i++) {
        cssText = rules[i].cssText;
        iconClass = selectorPlaceholder + rules[i].selectorText;
        iconSVGEncoded = cssText.split(');')[0].match(/US-ASCII,([^"']+)/);
        if (iconSVGEncoded && iconSVGEncoded[1]) {
            iconSVGRaw = decodeURIComponent(iconSVGEncoded[1]);
            // replace the SVG element opening tag with one that sets the focusable attribute to false.
            //  this was added to handle issues with web browers (IE) making svg elements focusable by default
            icons[iconClass] = iconSVGRaw.replace('<svg', '<svg focusable="false"');
        }
    }

    return icons;
}

const onloadCSS = (styleSheetElement, callback) => {
    let called;

    const loadCallBack = () => {
        if (!called && callback) {
            called = true;
            callback();
        }
    }

    styleSheetElement.crossorigin = 'anonymous';

    if (styleSheetElement.addEventListener) {
        styleSheetElement.addEventListener('load', loadCallBack);
    }

    if (styleSheetElement.attachEvent) {
        styleSheetElement.attachEvent('onload', loadCallBack);
    }

    // This code is for browsers that don’t support onload
    // No support for onload (it'll bind but never fire):
    //  * Android 4.3 (Samsung Galaxy S4, Browserstack)
    //  * Android 4.2 Browser (Samsung Galaxy SIII Mini GT-I8200L)
    //  * Android 2.3 (Pantech Burst P9070)

    // Weak inference targets Android < 4.4
    if ('isApplicationInstalled' in navigator && 'onloadcssdefined' in styleSheetElement) {
        styleSheetElement.onloadcssdefined(loadCallBack);
    }
}

const grunticon = options => {
    const { datasvg, datapng, urlpng } = options;

    const configuration = {};

    const svg = !!document.createElementNS
        && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect
        && !!document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1')
        && !(window.opera && navigator.userAgent.indexOf('Chrome') === -1)
        && navigator.userAgent.indexOf('Series40') === -1;

    const img = new Image();

    const loadIcons = () => {
        if (configuration.method !== 'svg') { return; }

        const stylesheet = getCSS(configuration.href);
        const icons = getIcons(stylesheet, selectorPlaceholder);

        iconStore = Object.assign({}, icons);
        deferredWork.forEach(work => work());
    };

    img.onerror = () => {
        configuration.method = 'png';
        configuration.href = urlpng;
        cssLoader(urlpng);
    };

    img.onload = () => {
        const data = img.width === 1 && img.height === 1;
        const href = data && svg ? datasvg : data ? datapng : urlpng;

        if (data && svg) {
            configuration.method = 'svg';
        } else if (data) {
            configuration.method = 'datapng';
        } else {
            configuration.method = 'png';
        }

        configuration.href = href;

        if (getCSS(href)) {
            loadIcons();
        } else {
            onloadCSS(cssLoader(href, null, null, { crossorigin: 'anonymous' }), loadIcons);
        }
    };

    img.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
}

export { grunticon, addWork, isPopulated, lookupIcon };
