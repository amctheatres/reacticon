# Reacticon

A React-compatible implementation of [Grunticon](http://www.grunticon.com/).  It still requires Grunticon to generate the output files; this just replaces the runtime piece, grunticon.loader.js.

## Why?

The SVG embedding component of Grunticon directly manipulates the DOM.  Thus, it does not play well with React as component rerenders would wipe the SVG elements out.

## Installation

```
npm install --save @amctheatres/reacticon
```

## Usage

```javascript
import Icon from '@amctheatres/reacticon';
import React from 'react';

Icon.initialize(
    '/path/to/icons.data.svg.css',
    '/path/to/icons.data.png.css',
    '/path/to/icons.fallback.css'
);

export default class SampleComponent extends React.Component {
    render() {
        return (
            <div>
                <p>Look, it's a reacticon!</p>
                <Icon className="icon_yourIcon" />
            </div>
        );
    }
}
```

## Additional Information

By default, an <i> tag will be created.  You may override this by passing a `tagName` property (`tagName="div"`).  Or, if you would like to set the tag type for ALL icons so they use it by default, you may pass it as a fourth parameter to the initialize function.

Note that the `data-grunticon-embed` attribute normally required by Grunticon is not required as that functionality is implicitly assumed by using this component.